var searchData=
[
  ['dimension',['Dimension',['../class_processor.html#ac6aa9a564b81278c9aeabd186ffec7b7',1,'Processor']]],
  ['dimensionchanged',['dimensionChanged',['../class_main_window.html#a79b198d9cfe2f0488af2c5895ac63492',1,'MainWindow']]],
  ['draw',['draw',['../class_processor.html#aaa01b4a8e0d804b14108f2d285384543',1,'Processor::draw()'],['../class_figure_widget_1_1_scene.html#acc26df557cf51a14b6d8505b335aa484',1,'FigureWidget::Scene::draw()']]],
  ['drawcircle',['drawCircle',['../class_figure_widget.html#ac15cbf1fa62f7e59b2f9e31bc65bad0e',1,'FigureWidget']]],
  ['drawline',['drawLine',['../class_figure_widget.html#a1cf0883335b8caf531118bf753b46b29',1,'FigureWidget']]],
  ['drawpolygon',['drawPolygon',['../class_figure_widget.html#ad022c83ce7cd1d3f9fc88d62149e0a8a',1,'FigureWidget']]],
  ['drawpolyline',['drawPolyline',['../class_figure_widget.html#a4a0241944c7ec8a4ec48df14f46e8fe4',1,'FigureWidget']]],
  ['drawtext',['drawText',['../class_figure_widget.html#a2917bf138d29df99b6b6de28c516f1fc',1,'FigureWidget']]],
  ['ds2vs',['ds2vs',['../class_figure_widget.html#a5d3f9f01060c276395a6969c55d63ed1',1,'FigureWidget']]],
  ['ds2ws',['ds2ws',['../class_figure_widget.html#a643f50108c78cfd8a2df26835f3402d1',1,'FigureWidget']]]
];

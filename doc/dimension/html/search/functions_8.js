var searchData=
[
  ['sceneupdated',['sceneUpdated',['../class_log_log_diagram.html#a204c4cd9c18c946b79b6f3140f380f73',1,'LogLogDiagram']]],
  ['setdimension',['setDimension',['../class_main_window.html#a777f916598ecbdd0799549db3f3e2b86',1,'MainWindow::setDimension()'],['../class_processor.html#afb477c2b8e73d8d4864651094b67de36',1,'Processor::setDimension()']]],
  ['setimage',['setImage',['../class_processor.html#a57bdb14085db879c2d3d81ef34587296',1,'Processor']]],
  ['setmargin',['setMargin',['../class_figure_widget.html#a84fe0db9f6533b02317f63af98778611',1,'FigureWidget']]],
  ['setrefinement',['setRefinement',['../class_main_window.html#a3998fd31d5b0952783178848284608d9',1,'MainWindow::setRefinement()'],['../class_processor.html#afa2029e4b00ca5a62fbe9c26c904ce75',1,'Processor::setRefinement()']]],
  ['setscene',['setScene',['../class_log_log_diagram.html#a46b63062c5d9617dec26159d49afa868',1,'LogLogDiagram::setScene()'],['../class_figure_widget.html#a8b8ada393adcf70aaf3b1752514c623d',1,'FigureWidget::setScene()']]],
  ['setspeed',['setSpeed',['../class_processor.html#a8fcbdfc7bf6d56d20d3c286d51b8020f',1,'Processor']]],
  ['setview',['setView',['../class_log_log_diagram.html#ae3e6be3bef56a8f225bdec2c6eb7fc4f',1,'LogLogDiagram::setView()'],['../class_figure_widget.html#ac70b7ba356e5fca830fcfe43db9aab08',1,'FigureWidget::setView()']]],
  ['sizehint',['sizeHint',['../class_processor.html#a1666cb4212d338d875f67dc271d6a8e1',1,'Processor::sizeHint()'],['../class_figure_widget.html#a1238009b9563575f19cc028a200cf0eb',1,'FigureWidget::sizeHint()']]],
  ['speedchanged',['speedChanged',['../class_main_window.html#a6a6e433f5f16587799ed5b6dba1001b5',1,'MainWindow']]],
  ['statechanged',['stateChanged',['../class_processor.html#a05edc7e062b2af0e4ea213ffa6e33b52',1,'Processor']]],
  ['step',['step',['../class_processor.html#aa6ee160a84a9b1ed2d79d81703f74bcd',1,'Processor']]],
  ['stop',['stop',['../class_processor.html#a96c4902b3169f62c96383c24899bce56',1,'Processor']]]
];

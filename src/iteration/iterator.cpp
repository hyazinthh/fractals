#include "iterator.h"

#include <QtMath>

// Maximum number of samples for function plotting
static constexpr int maxSamples = 4096;

// Desired number of samples per unit interval for function plotting
static constexpr int samplesPerUnit = 1000;

// Maximum view extent
static constexpr qreal maxViewSize = 10000;

// Minimum view extent
static constexpr qreal minViewSize = 0.001;

// Scale factor for zooming
static constexpr qreal zoomScaleFactor = 1.15;

// Color for selecting
static const QColor colSelecting = QColor(255, 165, 0, 100);

// Color for selected point / interval
static const QColor colSelected = QColor(0, 0, 255, 100);

// Epsilon constant for floating point comparison
static constexpr qreal epsilon = static_cast<qreal>(0.0001);

// Default time interval of animation
static constexpr int timerInterval = 1000;

// Minimum speed factor of the animation
static constexpr qreal timerMinFactor = static_cast<qreal>(0.25);

// Maximum speed factor of the animation
static constexpr qreal timerMaxFactor = static_cast<qreal>(4);

// Maximum orbit size
static constexpr int maxOrbitSize = 16;

Iterator::Iterator(QObject* parent)
    : QObject(parent) {

    functions.append(
        Function("ax · (1 - x)",
                 [] (qreal x, qreal a) {
                    return a * x * (1 - x);
                 },
                 [] (qreal) {
                    return Function::Interval(0, 1);
                 },
                 Function::Interval(0, 4))
    );

    functions.append(
        Function("a · sin(π · x)",
                 [] (qreal x, qreal a) {
                    return a * qSin(M_PI * x);
                 },
                 [] (qreal) {
                    return Function::Interval(0, 1);
                 },
                 Function::Interval(0, 1))
    );

    functions.append(
        Function("x · (1 + a · (1 - x))",
                 [] (qreal x, qreal a) {
                    return x * (1 + a * (1 - x));
                 },
                 [] (qreal a) {
                    return Function::Interval(0, (1 + a) / a);
                 },
                 Function::Interval(0, 1))
    );

    functions.append(
        Function("x · exp(a · (1 - x))",
                 [] (qreal x, qreal a) {
                    return x * qExp(a * (1 - x));
                 },
                 [] (qreal) {
                    return Function::Interval(0, Q_INFINITY);
                 },
                 Function::Interval(0, 8))
    );

    functions.append(
        Function("x · x + 0.25 + a",
                 [] (qreal x, qreal a) {
                    return x * x + 0.25 + a;
                 },
                 [] (qreal) {
                    return Function::Interval(-Q_INFINITY, Q_INFINITY);
                 },
                 Function::Interval(0, 0.5))
    );

    ptsFunc.reserve(maxSamples);
    ptsPoly.reserve(maxSamples);

    timer.setInterval(timerInterval);
    connect(&timer, &QTimer::timeout, this, &Iterator::computeStep);

    orbit.steps.reserve(maxOrbitSize);
}

void Iterator::setFunction(const Function& f) {

    this->f = f;
    transition(State::Idle);

    emit sceneUpdated(this);
}

void Iterator::setParam(double a) {

    this->a = static_cast<qreal>(a);

    if (state != State::Idle && (state != State::Selecting || statePrev != State::Idle)) {

        orbit.clear();

        if (state == State::Selecting) {
            statePrev = State::Stopped;
        } else {
            transition(State::Stopped);
        }
    }

    emit sceneUpdated(this);
}

void Iterator::setSpeed(qreal v) {

    qreal t = timerMinFactor + (1 - v) * (timerMaxFactor - timerMinFactor);
    timer.setInterval(qRound(t * timerInterval));
}

void Iterator::processMouseMove(FigureWidget* sender, QMouseEvent* event) {

    QRectF view = sender->getView();
    QRectF canvas = sender->getCanvasRect();

    QPointF dp = event->localPos() - lastMousePos;
    dp.setX((dp.x() / canvas.width()) * view.width());
    dp.setY((dp.y() / canvas.height()) * view.height());

    if ((event->buttons() & Qt::RightButton) ||
        (event->buttons() & Qt::LeftButton && state != State::Selecting)) {

        view.translate(-dp.x(), dp.y());
        lastMousePos = event->localPos();

        emit viewUpdated(view);
    }

    sender->update();
}

void Iterator::processMousePress(FigureWidget* sender, QMouseEvent* event) {

    // Select starting point
    if (state == State::Selecting && event->button() == Qt::LeftButton) {
        sel_x1 = sender->ws2ds(event->localPos()).x();
    }

    if (event->buttons() & (Qt::LeftButton | Qt::RightButton)) {
        lastMousePos = event->localPos();
    }

    sender->update();
}

void Iterator::processMouseRelease(FigureWidget* sender, QMouseEvent* event) {

    if (state == State::Selecting && event->button() == Qt::LeftButton) {
        sel_x2 = sender->ws2ds(event->localPos()).x();

        if (getSelection(sel_x1, sel_x2) != Selection::None) {

            auto d = f.getDomain(a);
            sel_x1 = qMax(d.min, qMin(d.max, sel_x1));
            sel_x2 = qMax(d.min, qMin(d.max, sel_x2));

            orbit.clear();
            orbit.start_x1 = sel_x1;
            orbit.start_x2 = sel_x2;

            transition(State::Stopped);
        } else {
            transition(State::Idle);
        }
    }

    sender->update();
}

void Iterator::processMouseWheel(FigureWidget* sender, QWheelEvent* event) {

    QRectF view = sender->getView();

    qreal s = zoomScaleFactor;

    if (event->angleDelta().y() > 0) {
        s = (view.width() > minViewSize)  ? (1 / s) : 1;
    } else {
        s = (view.width() < maxViewSize) ? s : 1;
    }

    QPointF c = sender->ws2ds(event->posF());

    view.translate(-c);
    view = QTransform::fromScale(s, s).mapRect(view);
    view.translate(c);

    emit viewUpdated(view);
}

void Iterator::select() {

    if (state != State::Selecting) {
        sel_x1 = sel_x2 = Q_SNAN;
        transition(State::Selecting);
    } else {
        transition(statePrev);
    }
}

void Iterator::play() {

    if (state != State::Iterating) {

        if (state == State::Stopped) {
            computeStep();
        }

        transition(State::Iterating);
    } else {
        transition(State::Paused);
    }
}

void Iterator::step() {
    if (state == State::Stopped) {
        transition(State::Paused);
    }

    computeStep();
}

void Iterator::stop() {

    orbit.clear();
    transition(State::Stopped);
}

void Iterator::transition(State stateNew) {

    if (state == stateNew) {
        return;
    }

    statePrev = state;
    state = stateNew;
    emit stateChanged(state);

    if (state == State::Iterating) {
        timer.start();
    } else {
        timer.stop();
    }
}

void Iterator::copySamples(QVector<QPointF>& buffer, qreal& xa, qreal& xb, qreal min, qreal max, int n) {

    int ia = qMax(0, qMin(n - 1, qRound((n - 1) * (xa - min) / (max - min))));
    int ib = qMax(0, qMin(n - 1, qRound((n - 1) * (xb - min) / (max - min))));

    if (ia > ib) {
        std::swap(ia, ib);
        std::swap(xa, xb);
    }

    buffer.resize((ib - ia + 1) + 4);
    std::copy(ptsFunc.begin() + ia, ptsFunc.begin() + ib + 1, buffer.begin() + 2);
}

void Iterator::preparePolygon(QVector<QPointF>& buffer, qreal xa, qreal xb, qreal min, qreal max, int n) {

    copySamples(buffer, xa, xb, min, max, n);

    buffer[0] = QPointF(xa, 0);
    buffer[1] = QPointF(xa, f(xa, a));
    buffer[buffer.size() - 2] = QPointF(xb, f(xb, a));
    buffer[buffer.size() - 1] = QPointF(xb, 0);
}

void Iterator::preparePolygonV(QVector<QPointF>& buffer, qreal xa, qreal xb, qreal min, qreal max, int n) {

    copySamples(buffer, xa, xb, min, max, n);

    buffer[0] = QPointF(xa, xa);
    buffer[1] = QPointF(xa, f(xa, a));
    buffer[buffer.size() - 2] = QPointF(xb, f(xb, a));
    buffer[buffer.size() - 1] = QPointF(xb, xb);
}

void Iterator::preparePolygonH(QVector<QPointF>& buffer, qreal xa, qreal xb, qreal min, qreal max, int n) {

    copySamples(buffer, xa, xb, min, max, n);

    buffer[0] = QPointF(f(xa, a), f(xa, a));
    buffer[1] = QPointF(xa, f(xa, a));
    buffer[buffer.size() - 2] = QPointF(xb, f(xb, a));
    buffer[buffer.size() - 1] = QPointF(f(xb, a), f(xb, a));
}

Iterator::Selection Iterator::getSelection(qreal a, qreal b, FigureWidget* w) const {

    auto d = f.getDomain(this->a);

    if (!qIsNaN(a) && !qIsNaN(b)) {
        if ((a < d.min && b < d.min) || (a > d.max && b > d.max)) {
            return Selection::None;
        } else {
            if (!w) {
                return (qAbs(b - a) < epsilon) ? Selection::Point : Selection::Interval;
            } else {
                return (wsDistX(w, a, b) < 1) ? Selection::Point : Selection::Interval;
            }
        }
    } else if (!qIsNaN(a)) {
        return (a < d.min || a > d.max) ? Selection::None : Selection::Point;
    } else if (!qIsNaN(b)) {
        return (b < d.min || b > d.max) ? Selection::None : Selection::Point;
    }

    return Selection::None;
}

void Iterator::computeStep() {

    // Allocate memory for next step
    int prev = orbit.pos;
    orbit.pos = (prev + 1) % maxOrbitSize;

    if (orbit.pos >= orbit.steps.size()) {
        orbit.steps.resize(orbit.pos + 1);
    }

    if (prev + 1 >= maxOrbitSize) {
        orbit.wrapped = true;
    }

    auto& s = orbit.steps[orbit.pos];

    // Compute y values
    s.x1 = (prev < 0) ? orbit.start_x1 : orbit.steps[prev].y_min;
    s.x2 = (prev < 0) ? orbit.start_x2 : orbit.steps[prev].y_max;
    s.y1 = f(s.x1, a);
    s.y2 = f(s.x2, a);

    // Check if values reached infinity
    if (!qIsFinite(s.y1) || !qIsFinite(s.y2)) {
        stop();
        return;
    }

    // Find the minimum and maximum y values and the corresponding
    // x values as they are needed for the next step.
    auto d = f.getDomain(a);
    s.y_min = d.max;
    s.y_max = d.min;

    int n = qMax(2, qMin(maxSamples, qRound(qAbs(s.x2 - s.x1) * samplesPerUnit)));

    for (int i = 0; i < n; i++) {
        qreal x = s.x1 + (static_cast<qreal>(i) / (n - 1)) * (s.x2 - s.x1);
        qreal y = f(x, a);

        if (y < s.y_min) {
            s.x_min = x;
            s.y_min = y;
        }

        if (y > s.y_max) {
            s.x_max = x;
            s.y_max = y;
        }
    }

    emit sceneUpdated(this);
}

void Iterator::draw(FigureWidget* w) {

    auto d = f.getDomain(a);
    const QRectF& v = w->getView();

    // Draw median
    qreal m1 = qMin(v.top(), v.left());
    qreal m2 = qMax(v.bottom(), v.right());

    w->drawLine(m1, m1, m2, m2, QColor(Qt::darkGreen));

    // Compute function samples
    qreal min = qMax(v.left(), d.min);
    qreal max = qMin(v.right(), d.max);

    if (min >= max) {
        return;
    }

    const int N = qMax(2, qMin(maxSamples, qRound((max - min) * samplesPerUnit)));
    ptsFunc.resize(N);

    for (int i = 0; i < N; i++) {
        ptsFunc[i].rx() = min + (static_cast<qreal>(i) / (N - 1)) * (max - min);
        ptsFunc[i].ry() = f(ptsFunc[i].x(), a);
    }

    // Draw selection
    if (state == State::Selecting) {

        qreal xc = w->ws2ds(w->mapFromGlobal(QCursor::pos())).x();
        auto sel = getSelection(sel_x1, xc, w);

        if (sel == Selection::Point) {
            w->drawLine(xc, 0, xc, f(xc, a), colSelecting);
        } else if (sel == Selection::Interval) {

            preparePolygon(ptsPoly,
                           qMax(d.min, qMin(d.max, sel_x1)),
                           qMax(d.min, qMin(d.max, xc)),
                           min, max, N);

            w->drawPolygon(ptsPoly, colSelecting);
        }

    } else if (state == State::Stopped) {

        if (wsDistX(w, orbit.start_x1, orbit.start_x2) < 1) {
            w->drawLine(orbit.start_x1, 0, orbit.start_x1, f(orbit.start_x1, a), colSelected);
        } else {
            preparePolygon(ptsPoly, orbit.start_x1, orbit.start_x2, min, max, N);
            w->drawPolygon(ptsPoly, colSelected);
        }

    } else if (state == State::Iterating || state == State::Paused) {

        for (int i = 0; i < orbit.steps.size(); i++) {

            int start = (orbit.pos + 1) % orbit.steps.size();
            int p = (start + i) % orbit.steps.size();
            auto& s = orbit.steps[p];

            // Draw line / interval from median to function
            if (wsDistX(w, s.x1, s.x2) < 1) {
                if (p == 0 && !orbit.wrapped) {
                   w->drawLine(s.x1, 0, s.x1, s.y1, colSelected);
                } else {
                   w->drawLine(s.x1, s.x1, s.x1, s.y1, colSelected);
                }
            } else {
                if (p == 0 && !orbit.wrapped) {
                    preparePolygon(ptsPoly, s.x1, s.x2, min, max, N);
                } else {
                    preparePolygonV(ptsPoly, s.x1, s.x2, min, max, N);
                }
                w->drawPolygon(ptsPoly, colSelected);
            }

            // Draw line / interval from function to median
            if (wsDistY(w, s.y_min, s.y_max) < 1) {
                w->drawLine(s.x1, s.y1, s.y1, s.y1, colSelected);
            } else {
                preparePolygonH(ptsPoly, s.x_min, s.x_max, min, max, N);
                w->drawPolygon(ptsPoly, colSelected);
            }
        }
    }

    // Draw function
    w->drawPolyline(ptsFunc);
}

#ifndef ITERATOR_H
#define ITERATOR_H

#include "function.h"
#include "figurewidget.h"
#include <QVector>
#include <QTimer>

/**
 * @brief Class for computing and visualizing graphical iteration of functions.
 */
class Iterator : public QObject, public FigureWidget::Scene
{
    Q_OBJECT

    public:

        /**
         * @brief Enum for the states of the iterator.
         */
        enum class State {
            Idle,
            Selecting,
            Paused,
            Stopped,
            Iterating
        };

        /**
         * @brief Constructs a new iterator.
         * @param parent the parent.
         */
        explicit Iterator(QObject* parent = nullptr);

        /**
         * @brief Gets all functions that are available for iteration.
         * @return the functions to be iterated.
         */
        const QVector<Function>& getFunctions() const {
            return functions;
        }

        /**
         * @brief Draws the iterator.
         * @param w the figure widget to be drawn on.
         */
        virtual void draw(FigureWidget* w) Q_DECL_OVERRIDE;

    signals:

        /**
         * @brief Emitted whenever the scene is updated and needs to be redrawn.
         * @param scene pointer to the updated scene.
         */
        void sceneUpdated(FigureWidget::Scene* scene);

        /**
         * @brief Emitted when the view is updated.
         * @param view the updated view.
         */
        void viewUpdated(const QRectF& view);

        /**
         * @brief Emitted when the state changed.
         * @param state the new state.
         */
        void stateChanged(State state);

    public slots:

        /**
         * @brief Sets the function to be iterated.
         * @param f the function.
         */
        void setFunction(const Function& f);

        /**
         * @brief Sets the parameter of the function.
         * @param a the parameter.
         */
        void setParam(double a);

        /**
         * @brief Handles mouse movement events.
         * @param sender the sender of the event.
         * @param event the event data.
         */
        void processMouseMove(FigureWidget* sender, QMouseEvent* event);

        /**
         * @brief Handles mouse press events.
         * @param sender the sender of the event.
         * @param event the event data.
         */
        void processMousePress(FigureWidget*, QMouseEvent* event);

        /**
         * @brief Handles mouse release events.
         * @param sender the sender of the event.
         * @param event the event data.
         */
        void processMouseRelease(FigureWidget*, QMouseEvent* event);

        /**
         * @brief Handles mouse wheel events.
         * @param sender the sender of the event.
         * @param event the event data.
         */
        void processMouseWheel(FigureWidget* sender, QWheelEvent* event);

        /**
         * @brief Starts or stops the selection process.
         */
        void select();

        /**
         * @brief Resumes or pauses the iteration.
         */
        void play();

        /**
         * @brief Advances the iteration by a single step.
         */
        void step();

        /**
         * @brief Resets the iteration to the starting point.
         */
        void stop();

        /**
         * @brief Sets the speed of the animation.
         * @param v the speed in the interval [0, 1].
         */
        void setSpeed(qreal v);

    private slots:

        /**
         * @brief Computes a single iteration.
         */
        void computeStep();

    private:

        /**
         * @brief Enumeration for selection types.
         */
        enum Selection {
            None,
            Point,
            Interval
        };

        /**
         * @brief Struct saving for saving orbits
         */
        struct Orbit {
            struct Step {
                qreal x1, x2, y1, y2;
                qreal x_min, x_max, y_min, y_max;
            };

            void clear() {
                pos = -1;
                wrapped = false;
                steps.resize(0);
            }

            int pos;
            bool wrapped;
            QVector<Step> steps;
            qreal start_x1, start_x2;
        };

        /**
         * @brief Changes the state.
         * @param stateNew the new state.
         */
        void transition(State stateNew);

        /**
         * @brief Copies function samples for drawing from the given interval to a buffer.
         * @param buffer the buffer used for storing the points.
         * @param xa the first interval boundary.
         * @param xb the second interval boundary.
         * @param min the minimum visible x-coordinate of the function.
         * @param max the maximum visible x-coordinate of the function.
         * @param n the number of samples used to draw the function.
         *
         * It is ensured that xa <= xb after the function returns.
         */
        void copySamples(QVector<QPointF>& buffer, qreal& xa, qreal& xb, qreal min, qreal max, int n);

        /**
         * @brief Prepares a buffer for drawing a polygon corresponding to the given interval.
         * @param buffer the buffer used for storing the points.
         * @param xa the first interval boundary.
         * @param xb the second interval boundary.
         * @param min the minimum visible x-coordinate of the function.
         * @param max the maximum visible x-coordinate of the function.
         * @param n the number of samples used to draw the function.
         */
        void preparePolygon(QVector<QPointF>& buffer, qreal xa, qreal xb, qreal min, qreal max, int n);


        /**
         * @brief Prepares a buffer for drawing a polygon from the median to the function.
         * @param buffer the buffer used for storing the points.
         * @param xa the first interval boundary.
         * @param xb the second interval boundary.
         * @param min the minimum visible x-coordinate of the function.
         * @param max the maximum visible x-coordinate of the function.
         * @param n the number of samples used to draw the function.
         */
        void preparePolygonV(QVector<QPointF>& buffer, qreal xa, qreal xb, qreal min, qreal max, int n);

        /**
         * @brief Prepares a buffer for drawing a polygon from the function to the median.
         * @param buffer the buffer used for storing the points.
         * @param xa the first interval boundary.
         * @param xb the second interval boundary.
         * @param min the minimum visible x-coordinate of the function.
         * @param max the maximum visible x-coordinate of the function.
         * @param n the number of samples used to draw the function.
         */
        void preparePolygonH(QVector<QPointF>& buffer, qreal xa, qreal xb, qreal min, qreal max, int n);

        /**
         * @brief Computes the distance between (@p x1, y) and (@p x2, y) in widget space.
         * @param w the widget.
         * @param x1 the first coordinate.
         * @param x2 the second coordinate.
         * @return the distance.
         */
        qreal wsDistX(FigureWidget* w, qreal x1, qreal x2) const {
            return qAbs(w->ds2ws(x1, 0).x() - w->ds2ws(x2, 0).x());
        }

        /**
         * @brief Computes the distance between (x, @p y1) and (x, @p y2) in widget space.
         * @param w the widget.
         * @param y1 the first coordinate.
         * @param y2 the second coordinate.
         * @return the distance.
         */
        qreal wsDistY(FigureWidget* w, qreal y1, qreal y2) const {
            return qAbs(w->ds2ws(0, y1).y() - w->ds2ws(0, y2).y());
        }

        /**
         * @brief Evaluates the type of selection defined by @p a and @p b.
         * @param a the first point.
         * @param b the second point.
         * @param w the figure widget to be drawn on, or null.
         * @return the type of selection.
         *
         * Evaluates if the @p a and @p b correspond to either a valid point
         * or interval selection on the x-axis. If @p w is not null, two points are
         * considered different if they correspond to two different points in
         * that figure.
         */
        Selection getSelection(qreal a, qreal b, FigureWidget* w = Q_NULLPTR) const;

        // Stores available functions
        QVector<Function> functions;

        // Current function
        Function f;

        // Current param
        qreal a;

        // Buffers storing function and polygon samples respectively
        QVector<QPointF> ptsFunc, ptsPoly;

        // The last position of the mouse
        QPointF lastMousePos;

        // Current state
        State state = State::Idle;

        // Previous state
        State statePrev = State::Idle;

        // Selected starting points
        qreal sel_x1, sel_x2;

        // Timer for steps
        QTimer timer;

        // Orbit of the iteration
        Orbit orbit;
};

#endif // ITERATOR_H

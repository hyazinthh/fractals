#-------------------------------------------------
#
# Project created by QtCreator 2018-02-13T18:03:33
#
#-------------------------------------------------

QT       += core gui
CONFIG   += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = iteration
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    main.cpp \
    mainwindow.cpp \
    iterator.cpp \

HEADERS += \
    mainwindow.h \
    function.h \
    iterator.h \

FORMS += \
    mainwindow.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../figurewidget/release/ -lfigurewidget
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../figurewidget/debug/ -lfigurewidget
else:unix: LIBS += -L$$OUT_PWD/../figurewidget/ -lfigurewidget

INCLUDEPATH += $$PWD/../figurewidget
DEPENDPATH += $$PWD/../figurewidget

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../figurewidget/release/libfigurewidget.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../figurewidget/debug/libfigurewidget.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../figurewidget/release/figurewidget.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../figurewidget/debug/figurewidget.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../figurewidget/libfigurewidget.a

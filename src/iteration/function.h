#ifndef FUNCTION_H
#define FUNCTION_H

#include <QString>
#include <QPair>
#include <QMetaType>
#include <functional>

/**
 * @brief Class representing 1D functions with a single parameter.
 */
class Function { 

    public:

        /**
         * @brief Struct representing a real interval.
         */
        struct Interval {
            /**
             * @brief Default constructor.
             */
            Interval() = default;

            /**
             * @brief Constructs a new interval.
             * @param a the lower bound.
             * @param b the upper bound.
             */
            Interval(qreal a, qreal b)
                : min(a), max(b) {
            }

            /**
             * @brief Gets the size of the interval.
             * @return the size.
             */
            inline qreal size() {
                return max - min;
            }

            // The interval bounds
            qreal min = 0, max = 0;
        };

        /**
         * @brief Wrapper for the function itself.
         */
        typedef std::function<qreal (qreal, qreal)> Wrapper;

        /**
         * @brief Domain based on the parameter.
         */
        typedef std::function<Interval (qreal)> DomainFunc;

        /**
         * @brief Default constructor.
         */
        Function() {
            f = [] (qreal, qreal) { return qreal(0); };
            domain = [] (qreal) { return Interval(0, 0); };
        }

        /**
         * @brief Constructs a new function.
         * @param name human readable name for the function.
         * @param func the function itself.
         * @param domain the domain of the function.
         * @param paramDomain the domain of the parameter.
         */
        Function(const QString& name, Wrapper func, DomainFunc domain, Interval paramDomain)
            : name(name), f(func), domain(domain), paramDomain(paramDomain) {
        }

        /**
         * @brief Gets the name of the function.
         * @return the name.
         */
        const QString& getName() const {
            return name;
        }

        /**
         * @brief Gets the domain of the function, given the parameter.
         * @param a the parameter of the function.
         * @return the domain.
         */
        inline Interval getDomain(qreal a) const {
            return domain(a);
        }

        /**
         * @brief Gets the domain of the parameter.
         * @return the parameter domain.
         */
        inline Interval getParameterDomain() const {
            return paramDomain;
        }

        /**
         * @brief Evaluates the function at @p x given the parameter @p a.
         * @param x the position of evaluation.
         * @param a the parameter.
         * @return the function value at @p x.
         */
        inline float operator()(float x, float a) const {
            return f(x, a);
        }

    private:

        // Human readable describtion of the function
        QString name;

        // The function itself
        Wrapper f;

        // The domain of the function (dependent on a)
        DomainFunc domain;

        // Interval of the parameter a
        Interval paramDomain;

};

Q_DECLARE_METATYPE(Function)

#endif // FUNCTION_H

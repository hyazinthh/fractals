#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "function.h"
#include "iterator.h"

namespace Ui {
class MainWindow;
}

/**
 * @brief The main window.
 */
class MainWindow : public QMainWindow {

    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    signals:
        /**
         * @brief Emitted whenever a new function is selected.
         * @param f the new function.
         */
        void functionChanged(const Function& f);

        /**
         * @brief Emitted whenever the parameter is changed.
         * @param a the new parameter.
         */
        void paramChanged(qreal a);

        /**
         * @brief Emitted whenever the speed is changed.
         * @param v the new speed.
         */
        void speedChanged(qreal v);

    public slots:
        /**
         * @brief Sets the function of the iterator based on the index of the combobox.
         * @param index the index of the selected function.
         */
        void setFunction(int index);

        /**
         * @brief Computes the parameter based on the slider value and emits a paramChanged() signal.
         * @param value the value of the slider.
         */
        void updateParam(int value);

        /**
         * @brief Computes the speed based on the slider value and emits a speedChanged() signal.
         * @param value the value of the slider.
         */
        void updateSpeed(int value);

        /**
         * @brief Updates the text of the parameter label.
         * @param a the parameter value.
         */
        void updateParamLabel(qreal a);

        /**
         * @brief Updates the controls according to the state of the iterator.
         * @param state the current state.
         */
        void updateControls(Iterator::State state);

    private:
        Ui::MainWindow *ui;

        QIcon iconPlay, iconPause;
};

#endif // MAINWINDOW_H

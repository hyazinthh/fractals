#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "figurewidget.h"
#include "iterator.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow) {

    ui->setupUi(this);

    // Create iterator
    Iterator* iterator = new Iterator(this);

    // Create figure widget
    FigureWidget* fw = new FigureWidget(this);
    QGridLayout* l = dynamic_cast<QGridLayout*>(ui->centralWidget->layout());
    l->addWidget(fw, 0, 0, -1, 1);

    // Setup signals / slots
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::close);
    connect(ui->comboBoxFunction, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MainWindow::setFunction);
    connect(ui->sliderParam, &QSlider::valueChanged, this, &MainWindow::updateParam);
    connect(this, &MainWindow::functionChanged, iterator, &Iterator::setFunction);
    connect(this, &MainWindow::paramChanged, iterator, &Iterator::setParam);
    connect(this, &MainWindow::paramChanged, this, &MainWindow::updateParamLabel);
    connect(iterator, &Iterator::viewUpdated, fw, &FigureWidget::setView);
    connect(iterator, &Iterator::sceneUpdated, fw, &FigureWidget::setScene);
    connect(iterator, &Iterator::stateChanged, fw, QOverload<>::of(&FigureWidget::update));
    connect(iterator, &Iterator::stateChanged, this, &MainWindow::updateControls);
    connect(fw, &FigureWidget::mouseMoved, iterator, &Iterator::processMouseMove);
    connect(fw, &FigureWidget::mousePressed, iterator, &Iterator::processMousePress);
    connect(fw, &FigureWidget::mouseReleased, iterator, &Iterator::processMouseRelease);
    connect(fw, &FigureWidget::mouseWheelMoved, iterator, &Iterator::processMouseWheel);
    connect(ui->buttonSelect, &QPushButton::clicked, iterator, &Iterator::select);
    connect(ui->buttonPlay, &QPushButton::clicked, iterator, &Iterator::play);
    connect(ui->buttonStep, &QPushButton::clicked, iterator, &Iterator::step);
    connect(ui->buttonStop, &QPushButton::clicked, iterator, &Iterator::stop);
    connect(this, &MainWindow::speedChanged, iterator, &Iterator::setSpeed);
    connect(ui->sliderSpeed, &QSlider::valueChanged, this, &MainWindow::updateSpeed);

    // Setup icons
    iconPlay = style()->standardIcon(QStyle::SP_MediaPlay);
    iconPause = style()->standardIcon(QStyle::SP_MediaPause);

    ui->buttonPlay->setIcon(iconPlay);
    ui->buttonStep->setIcon(style()->standardIcon(QStyle::SP_MediaSeekForward));
    ui->buttonStop->setIcon(style()->standardIcon(QStyle::SP_MediaStop));

    // Fill function combobox
    for (const Function& f : iterator->getFunctions()) {
        ui->comboBoxFunction->addItem(f.getName(), QVariant::fromValue(f));
    }
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::setFunction(int index) {
    if (index == -1) {
        return;
    }

    QVariant data = ui->comboBoxFunction->itemData(index);
    Function f = data.value<Function>();

    emit functionChanged(f);
    updateParam(ui->sliderParam->value());
}

void MainWindow::updateParam(int value) {

    int min = ui->sliderParam->minimum();
    int max = ui->sliderParam->maximum();
    qreal a = static_cast<qreal>(value - min) / (max - min);

    QVariant data = ui->comboBoxFunction->currentData();
    auto d = data.value<Function>().getParameterDomain();

    emit paramChanged(d.min + a * d.size());
}

void MainWindow::updateSpeed(int value) {

    int min = ui->sliderSpeed->minimum();
    int max = ui->sliderSpeed->maximum();
    emit speedChanged(static_cast<qreal>(value - min) / (max - min));
}

void MainWindow::updateParamLabel(qreal a) {
    ui->labelParam->setText("a = " + QLocale().toString(a));
}

void MainWindow::updateControls(Iterator::State state) {

    switch (state) {
        case Iterator::State::Idle:
            ui->buttonPlay->setEnabled(false);
            ui->buttonStep->setEnabled(false);
            ui->buttonStop->setEnabled(false);
            ui->buttonPlay->setIcon(iconPlay);
            ui->buttonSelect->setText(tr("Select"));
            break;
        case Iterator::State::Selecting:
            ui->buttonPlay->setEnabled(false);
            ui->buttonStep->setEnabled(false);
            ui->buttonStop->setEnabled(false);
            ui->buttonPlay->setIcon(iconPlay);
            ui->buttonSelect->setText(tr("Cancel"));
            break;
        case Iterator::State::Paused:
            ui->buttonPlay->setEnabled(true);
            ui->buttonStep->setEnabled(true);
            ui->buttonStop->setEnabled(true);
            ui->buttonPlay->setIcon(iconPlay);
            ui->buttonSelect->setText(tr("Select"));
            break;
        case Iterator::State::Stopped:
            ui->buttonPlay->setEnabled(true);
            ui->buttonStep->setEnabled(true);
            ui->buttonStop->setEnabled(false);
            ui->buttonPlay->setIcon(iconPlay);
            ui->buttonSelect->setText(tr("Select"));
            break;
        case Iterator::State::Iterating:
            ui->buttonPlay->setEnabled(true);
            ui->buttonStep->setEnabled(true);
            ui->buttonStop->setEnabled(true);
            ui->buttonPlay->setIcon(iconPause);
            ui->buttonSelect->setText(tr("Select"));
    }
}

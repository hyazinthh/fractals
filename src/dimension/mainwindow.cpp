#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {

    ui->setupUi(this);

    // Create processor
    Processor* p = new Processor(this);
    QGridLayout* l = dynamic_cast<QGridLayout*>(ui->centralWidget->layout());
    l->addWidget(p, 1, 0, -1, 1);
    p->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    // Setup signals / slots
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::close);
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::loadImage);
    connect(ui->buttonLoad, &QPushButton::clicked, this, &MainWindow::loadImage);
    connect(ui->comboBoxDim, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MainWindow::setDimension);
    connect(this, &MainWindow::dimensionChanged, p, &Processor::setDimension);
    connect(ui->comboBoxRefinement, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MainWindow::setRefinement);
    connect(this, &MainWindow::refinementChanged, p, &Processor::setRefinement);
    connect(this, &MainWindow::imageLoaded, p, &Processor::setImage);
    connect(p, &Processor::stateChanged, this, &MainWindow::updateControls);
    connect(ui->buttonPlay, &QPushButton::clicked, p, &Processor::play);
    connect(ui->buttonStep, &QPushButton::clicked, p, &Processor::step);
    connect(ui->buttonStop, &QPushButton::clicked, p, &Processor::stop);
    connect(this, &MainWindow::speedChanged, p, &Processor::setSpeed);
    connect(ui->sliderSpeed, &QSlider::valueChanged, this, &MainWindow::updateSpeed);

    // Setup icons
    iconPlay = style()->standardIcon(QStyle::SP_MediaPlay);
    iconPause = style()->standardIcon(QStyle::SP_MediaPause);

    ui->buttonPlay->setIcon(iconPlay);
    ui->buttonStep->setIcon(style()->standardIcon(QStyle::SP_MediaSeekForward));
    ui->buttonStop->setIcon(style()->standardIcon(QStyle::SP_MediaStop));

    // Add elements to combo boxes
    for (const auto& desc : p->getSupportedDimensions()) {
        ui->comboBoxDim->addItem(desc.second, QVariant::fromValue(desc.first));
    }

    for (const auto& desc : p->getSupportedRefinements()) {
        ui->comboBoxRefinement->addItem(desc.second, QVariant::fromValue(desc.first));
    }
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::loadImage() {

    QString fname = QFileDialog::getOpenFileName(this,
                                                 tr("Load image"), "./img/",
                                                 tr("Image Files (*.png *.jpg *.bmp *.gif)"));

    if (!fname.isNull()) {

        QImage img(fname);

        if (img.isNull()) {
            ui->labelStatus->setText(tr("Failed to load image '%1'").arg(fname));
            emit imageLoaded(QImage());
        } else {
            ui->labelStatus->setText(tr("Loaded image '%1'").arg(fname));
            emit imageLoaded(img);
        }
    }
}

void MainWindow::setDimension(int index) {

    if (index == -1) {
        return;
    }

    QVariant data = ui->comboBoxDim->itemData(index);
    Processor::Dimension d = data.value<Processor::Dimension>();

    emit dimensionChanged(d);
}

void MainWindow::setRefinement(int index) {

    if (index == -1) {
        return;
    }

    QVariant data = ui->comboBoxRefinement->itemData(index);
    Processor::Refinement r = data.value<Processor::Refinement>();

    emit refinementChanged(r);
}

void MainWindow::updateSpeed(int value) {

    int min = ui->sliderSpeed->minimum();
    int max = ui->sliderSpeed->maximum();
    emit speedChanged(static_cast<qreal>(value - min) / (max - min));
}

void MainWindow::updateControls(Processor::State state) {

    switch (state) {
        case Processor::State::Idle:
            ui->buttonPlay->setEnabled(false);
            ui->buttonStep->setEnabled(false);
            ui->buttonStop->setEnabled(false);
            ui->buttonPlay->setIcon(iconPlay);
            break;
        case Processor::State::Paused:
            ui->buttonPlay->setEnabled(true);
            ui->buttonStep->setEnabled(true);
            ui->buttonStop->setEnabled(true);
            ui->buttonPlay->setIcon(iconPlay);
            break;
        case Processor::State::Stopped:
            ui->buttonPlay->setEnabled(true);
            ui->buttonStep->setEnabled(true);
            ui->buttonStop->setEnabled(false);
            ui->buttonPlay->setIcon(iconPlay);
            break;
        case Processor::State::Running:
            ui->buttonPlay->setEnabled(true);
            ui->buttonStep->setEnabled(true);
            ui->buttonStop->setEnabled(true);
            ui->buttonPlay->setIcon(iconPause);
            break;
    }
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "processor.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

/**
 * @brief The main window.
 */
class MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    public slots:
        /**
         * @brief Prompts the user to load an image file.
         */
        void loadImage();

        /**
         * @brief Sets the dimension based on the index of the combobox.
         * @param index the index of the selected dimension.
         */
        void setDimension(int index);

        /**
         * @brief Sets the grid refinment based on the index of the combobox.
         * @param index the index of the selected refinement.
         */
        void setRefinement(int index);

        /**
         * @brief Computes the speed based on the slider value and emits a speedChanged() signal.
         * @param value the value of the slider.
         */
        void updateSpeed(int value);

        /**
         * @brief Updates the controls according to the state of the processor.
         * @param state the current state.
         */
        void updateControls(Processor::State state);

    signals:
        /**
         * @brief Emitted whenever an image has been loaded.
         * @param img the loaded image.
         */
        void imageLoaded(const QImage& img);

        /**
         * @brief Emitted whenever the dimension is changed.
         * @param d the new dimension.
         */
        void dimensionChanged(Processor::Dimension d);

        /**
         * @brief Emitted whenever the grid refinement is changed.
         * @param r the new refinement.
         */
        void refinementChanged(Processor::Refinement r);

        /**
         * @brief Emitted whenever the speed is changed.
         * @param v the new speed.
         */
        void speedChanged(qreal v);

    private:
        Ui::MainWindow *ui;

        QIcon iconPlay, iconPause;
};

#endif // MAINWINDOW_H

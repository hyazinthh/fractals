#ifndef LOGLOGDIAGRAM_H
#define LOGLOGDIAGRAM_H

#include "figurewidget.h"

#include <QDialog>

/**
 * @brief Dialog for log / log diagrams.
 */
class LogLogDiagram : public QDialog {
        Q_OBJECT

    public:
        /**
         * @brief Creates a new log / log diagram dialog.
         * @param labelX the label for the x-axis.
         * @param labelY the label for the y-axis.
         * @param parent the parent of the dialog.
         */
        explicit LogLogDiagram(const QString& labelX, const QString& labelY, QWidget* parent = Q_NULLPTR);

    signals:

        /**
         * @brief Emitted whenever the scene is updated and needs to be redrawn.
         * @param scene pointer to the updated scene.
         */
        void sceneUpdated(FigureWidget::Scene* scene);

        /**
         * @brief Emitted when the view is updated.
         * @param view the updated view.
         */
        void viewUpdated(const QRectF& view);

    public slots:

        /**
         * @brief Sets the pointer to the scene to render.
         * @param scene the scene.
         */
        void setScene(FigureWidget::Scene* scene) {
            emit sceneUpdated(scene);
        }

        /**
         * @brief Sets the current view.
         * @param view
         */
        void setView(const QRectF& view) {
            emit viewUpdated(view);
        }

    private slots:

        /**
         * @brief Handles mouse movement events.
         * @param sender the sender of the event.
         * @param event the event data.
         */
        void processMouseMove(FigureWidget* sender, QMouseEvent* event);

        /**
         * @brief Handles mouse press events.
         * @param sender the sender of the event.
         * @param event the event data.
         */
        void processMousePress(FigureWidget*, QMouseEvent* event);

        /**
         * @brief Handles mouse wheel events.
         * @param sender the sender of the event.
         * @param event the event data.
         */
        void processMouseWheel(FigureWidget* sender, QWheelEvent* event);

    private:

        // The last position of the mouse
        QPointF lastMousePos;
};

#endif // LOGLOGDIAGRAM_H

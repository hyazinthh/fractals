#include "processor.h"

#include <QPainter>
#include <QLocale>
#include <QtMath>
#include <cmath>
#include <algorithm>

#include "loglogdiagram.h"

// Initial grid resolution
static constexpr int cellCountStart = 4;

// Maximum number of steps
static constexpr int stepsMax = 10000;

// Default time interval of animation
static constexpr int timerInterval = 500;

// Minimum speed factor of the animation
static constexpr qreal timerMinFactor = static_cast<qreal>(0.25);

// Maximum speed factor of the animation
static constexpr qreal timerMaxFactor = static_cast<qreal>(3);

// Largest real value
static constexpr qreal realMax = std::numeric_limits<qreal>::max();

// Smallest real value
static constexpr qreal realMin = std::numeric_limits<qreal>::lowest();

// Color for non empty cell
static const QColor colCell = QColor(0, 0, 255, 100);

Processor::Processor(QWidget* parent)
    : QWidget(parent) {

    supportedDimensions.append(DimensionDesc(Dimension::BoxCount, tr("Box count")));
    supportedDimensions.append(DimensionDesc(Dimension::Information, tr("Information")));

    supportedRefinements.append(RefinementDesc(Refinement::Linear, tr("Linear")));
    supportedRefinements.append(RefinementDesc(Refinement::Exponential, tr("Exponential")));

    timer.setInterval(timerInterval);
    connect(&timer, &QTimer::timeout, this, &Processor::computeStep);
}

void Processor::setImage(const QImage& img) {

    image = img.convertToFormat(QImage::Format_RGB32);

    if (image.isNull()) {
        transition(State::Idle);
    } else {

        structurePixels = 0;

        for (int x = 0; x < image.width(); x++) {
            for (int y = 0; y < image.height(); y++) {
                structurePixels += pixel(x, y) ? 1 : 0;
            }
        }

        if (structurePixels > 0) {
            stop();
        } else {
            image = QImage();
            transition(State::Idle);
        }
    }

    update();
}

void Processor::setDimension(Dimension d) {
    dimension = d;

    if (state != State::Idle) {
        stop();
    }
}

void Processor::setRefinement(Refinement r) {
    refinement = r;

    if (state != State::Idle) {
        stop();
    }
}

void Processor::setSpeed(qreal v) {

    qreal t = timerMinFactor + (1 - v) * (timerMaxFactor - timerMinFactor);
    timer.setInterval(qRound(t * timerInterval));
}

void Processor::play() {

    if (state != State::Running) {
        transition(State::Running);
    } else {
        transition(State::Paused);
    }
}

void Processor::step() {
    if (state == State::Stopped) {
        transition(State::Paused);
    }

    computeStep();
}

void Processor::stop() {

    cells.clear();
    counts.clear();
    cellSize = qCeil(qMin(image.width(), image.height()) / static_cast<qreal>(cellCountStart));
    cellCount = getCellCount();

    transition(State::Stopped);
    update();
}

bool Processor::pixel(int x, int y) const {

    if (image.isNull()) {
        return false;
    }

    const QRgb* data = reinterpret_cast<const QRgb*>(image.constBits());

    if (!image.valid(x, y)) {
        return false;
    } else {
        QRgb c = data[y * image.width() + x];
        return qRed(c) + qBlue(c) + qGreen(c) == 0;
    }
}

void Processor::transition(State stateNew) {

    if (state == stateNew) {
        return;
    }

    state = stateNew;
    emit stateChanged(state);

    if (state == State::Running) {
        timer.start();
    } else {
        timer.stop();
    }
}

QSize Processor::getCellCount() const {

    return QSize(qCeil(image.width() / static_cast<qreal>(cellSize)),
                 qCeil(image.height() / static_cast<qreal>(cellSize)));
}

qreal Processor::processCell(int i, int j) {

    // Get cell rect
    QRect c(i * cellSize, j * cellSize,
            cellSize, cellSize);

    // Count non empty pixels
    int count = 0;

    for (int x = c.left(); x <= c.right(); x++) {
        for (int y = c.top(); y <= c.bottom(); y++) {

            if (!pixel(x, y)) {
                continue;
            }

            // If we compute box counting dimension we only
            // care if the cell is empty or not
            if (dimension == Dimension::BoxCount) {
                cells.append(CellInfo(i, j, 1));
                return 1;
            } else {
                ++count;
            }
        }
    }

    // Return percentage
    qreal w = count / static_cast<qreal>(structurePixels);
    cells.append(CellInfo(i, j, count / static_cast<qreal>(cellSize * cellSize)));

    return w;
}

void Processor::showResults() {

    // Compute initial view
    QPointF min(realMax, realMax), max(realMin, realMin);

    for (const QPointF& p : counts) {

        min = QPointF(qMin(p.x(), min.x()), qMin(p.y(), min.y()));
        max = QPointF(qMax(p.x(), max.x()), qMax(p.y(), max.y()));
    }

    qreal l = qMax<qreal>(1, qMax(max.x() - min.x(), max.y() - min.y()));

    QRectF view;
    view.setTopLeft(min);
    view.setSize(QSizeF(l, l));

    // Show diagram
    QString labelY((dimension == Dimension::BoxCount) ? "log N(s)" : "I(s)");

    LogLogDiagram* lld = new LogLogDiagram("log(1/s)", labelY, this);
    lld->setScene(this);
    lld->setView(view);

    lld->exec();
    delete lld;

    stop();
}

bool Processor::regressionLine(qreal& k, qreal& d) {

    // Fails if less than 2 points
    int n = counts.size();

    if (n < 2) {
        return false;
    }

    // Compute mean, xi*xi and xi*yi
    QVector2D m;
    qreal xx = 0, xy = 0;

    for (const QPointF& p : counts) {
        m += QVector2D(p) / n;
        xx += p.x() * p.x();
        xy += p.x() * p.y();
    }

    // Compute k and d
    k = (xy - n * m.x() * m.y()) / (xx - n * m.x() * m.x());
    d = m.y() - k * m.x();

    return true;
}

QPointF Processor::is2ws(const QPointF& p) const {

    qreal u = p.x() / image.width();
    qreal v = p.y() / image.height();

    return QPointF(u * width(), v * height());
}

void Processor::computeStep() {

    // Decrease cell width
    if (counts.size() > 0) {
        if (cellSize > 1 && counts.size() < stepsMax) {

            if (refinement == Refinement::Linear) {
                cellSize = qMax(1, cellSize - 2);
            } else {
                cellSize = qCeil(cellSize / 2);
            }

            cellCount = getCellCount();
        } else {
            showResults();
            return;
        }
    }

    // Process cells
    cells.clear();
    qreal count = 0;

    for (int i = 0; i < cellCount.width(); i++) {
        for (int j = 0; j < cellCount.height(); j++) {

            qreal mu = processCell(i, j);

            if (dimension == Dimension::BoxCount) {
                count += mu;
            } else if (mu > 0) {
                count += mu * qLn(1 / mu);
            }
        }
    }

    if (dimension == Dimension::BoxCount) {
        count = qLn(count);
    }

    counts.append(QPointF(qLn(1.0 / cellSize), count));

    update();
}

void Processor::paintEvent(QPaintEvent*) {

    QPainter painter(this);

    // Draw image
    if (!image.isNull()) {
        QImage i = image.scaled(this->rect().size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
        painter.drawImage(this->rect(), i);
    }

    // Draw cells
    painter.setBrush(colCell);
    painter.setPen(Qt::NoPen);

    for (const auto& c : cells) {
        QRectF r(c.i * cellSize, c.j * cellSize,
                 cellSize, cellSize);

        r.setTopLeft(is2ws(r.topLeft()));
        r.setBottomRight(is2ws(r.bottomRight()));

        // Based on the weight and a logarithmic scale, compute
        // the alpha value for the cell
        qreal a = std::log2(1 + c.weight * 7) / 3;

        QColor col = colCell;
        col.setAlphaF(a * col.alphaF());
        painter.setBrush(col);

        painter.drawRect(r);
    }

    // Draw grid
    if (state != State::Idle && state != State::Stopped) {

        painter.setBrush(Qt::NoBrush);
        painter.setPen(Qt::black);

        for (int i = 1; i < cellCount.width(); i++) {
            painter.drawLine(is2ws(i * cellSize, 0), is2ws(i * cellSize, image.height()));
        }

        for (int i = 1; i < cellCount.height(); i++) {
            painter.drawLine(is2ws(0, i * cellSize), is2ws(image.width(), i * cellSize));
        }
    }
}

void Processor::drawRegressionLineLabel(FigureWidget* w, qreal k, qreal d) {

    QPointF pm = std::accumulate(counts.begin(), counts.end(), QPointF(), [this] (const QPointF& a, const QPointF& p) {
        return a + p / counts.size();
    });

    qreal l = -1 / k;
    qreal e = pm.y() - l * pm.x();

    QPointF pi;
    pi.rx() = (e - d) / (k - l);
    pi.ry() = k * pi.x() + d;

    QTextOption options;
    options.setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    options.setWrapMode(QTextOption::NoWrap);

    QString text = "k = " + QLocale().toString(k, 'g', 4);
    w->drawText(w->vs2ws(w->ds2vs(pi) + QPointF(0.05, 0)), text, options);
}

void Processor::draw(FigureWidget* w) {

    const QRectF& view = w->getView();

    // Draw regression line
    qreal k, d;

    if (regressionLine(k, d)) {
        w->drawLine(view.left(), k * view.left() + d,
                    view.right(), k * view.right() + d,
                    colCell, true);
    }

    // Draw points
    for (const auto& p : counts) {
        w->drawCircle(p, 1);
    }

    // Draw text
    drawRegressionLineLabel(w, k, d);
}

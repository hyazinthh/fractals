#-------------------------------------------------
#
# Project created by QtCreator 2018-02-23T21:59:46
#
#-------------------------------------------------

QT       += core gui
CONFIG   += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dimension
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    processor.cpp \
    loglogdiagram.cpp

HEADERS  += mainwindow.h \
    processor.h \
    loglogdiagram.h

FORMS    += mainwindow.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../figurewidget/release/ -lfigurewidget
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../figurewidget/debug/ -lfigurewidget
else:unix: LIBS += -L$$OUT_PWD/../figurewidget/ -lfigurewidget

INCLUDEPATH += $$PWD/../figurewidget
DEPENDPATH += $$PWD/../figurewidget

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../figurewidget/release/libfigurewidget.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../figurewidget/debug/libfigurewidget.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../figurewidget/release/figurewidget.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../figurewidget/debug/figurewidget.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../figurewidget/libfigurewidget.a

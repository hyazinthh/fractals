#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QWidget>
#include <QImage>
#include <QTimer>
#include <QVector>
#include <QMap>

#include "figurewidget.h"

/**
 * @brief Class implementing the logic of computing the box counting
 * and information dimension of an image.
 */
class Processor : public QWidget, public FigureWidget::Scene {
    Q_OBJECT

    public:

        /**
         * @brief Enum for dimension types.
         */
        enum class Dimension {
            BoxCount,
            Information
        };

        typedef QPair<Dimension, QString> DimensionDesc;

        /**
         * @brief Enum for grid refinement types.
         */
        enum class Refinement {
            Linear,
            Exponential
        };

        typedef QPair<Refinement, QString> RefinementDesc;

        /**
         * @brief Enum for the states of the processor.
         */
        enum class State {
            Idle,
            Paused,
            Stopped,
            Running,
        };

        /**
         * @brief Constructs a new processor.
         * @param parent the parent of the widget.
         */
        explicit Processor(QWidget* parent = Q_NULLPTR);

        /**
         * @brief Returns the minimum size of the widget.
         * @return the minimum size.
         */
        QSize minimumSizeHint() const Q_DECL_OVERRIDE {
            return QSize(700, 700);
        }

        /**
         * @brief Returns the preferred size of the widget.
         * @return the prefrerred size.
         */
        QSize sizeHint() const Q_DECL_OVERRIDE {

            if (image.isNull()) {
                return minimumSizeHint();
            } else {
                return image.size();
            }
        }

        /**
         * @brief Gets the dimensions supported by the processor.
         * @return the supported dimensions.
         */
        const QVector<DimensionDesc>& getSupportedDimensions() const {
            return supportedDimensions;
        }


        /**
         * @brief Gets the grid refinements supported by the processor.
         * @return the supported refinements.
         */
        const QVector<RefinementDesc>& getSupportedRefinements() const {
            return supportedRefinements;
        }

        /**
         * @brief Draws the log/log diagram.
         * @param w the figure widget to be drawn on.
         */
        virtual void draw(FigureWidget* w) Q_DECL_OVERRIDE;

    signals:

        /**
         * @brief Emitted when the state changed.
         * @param state the new state.
         */
        void stateChanged(State state);

    public slots:

        /**
         * @brief Sets the image to process.
         * @param img the image.
         */
        void setImage(const QImage& img);

        /**
         * @brief Sets the dimension to compute.
         * @param d the dimension.
         */
        void setDimension(Dimension d);

        /**
         * @brief Sets the grid refinement strategy.
         * @param r the refinement.
         */
        void setRefinement(Refinement r);

        /**
         * @brief Sets the speed of the animation.
         * @param v the speed in the interval [0, 1].
         */
        void setSpeed(qreal v);

        /**
         * @brief Resumes or pauses the computation.
         */
        void play();

        /**
         * @brief Advances the computation by a single step.
         */
        void step();

        /**
         * @brief Resets the computation to the starting point.
         */
        void stop();

    protected:

        /**
         * @brief Called when widget is repainted.
         * @param event the event data.
         */
        void paintEvent(QPaintEvent* event) Q_DECL_OVERRIDE;

    private slots:

        /**
         * @brief Computes a single step.
         */
        void computeStep();

    private:

        struct CellInfo {
            CellInfo() = default;

            CellInfo(int i, int j, qreal weight)
                : i(i), j(j), weight(weight) {
            }

            int i, j;
            qreal weight;
        };

        /**
         * @brief Evaluates if the given pixel is part of the fractal structure.
         * @param x the x-coordinate of the pixel.
         * @param y the y-coordinate of the pixel.
         * @return true if the pixel is black, false otherwise or if out of bounds.
         */
        bool pixel(int x, int y) const;

        /**
         * @brief Changes the state.
         * @param stateNew the new state.
         */
        void transition(State stateNew);

        /**
         * @brief Computes the cell count for the current cell size.
         * @return the cell count.
         */
        QSize getCellCount() const;

        /**
         * @brief Processes the given cell.
         * @param i the cell column.
         * @param j the cell row.
         * @return the cell weight.
         */
        qreal processCell(int i, int j);

        /**
         * @brief Shows the results in a log / log diagram.
         */
        void showResults();

        /**
         * @brief Computes a regression line for the recorded counts.
         * @param k returns the slope of the line.
         * @param d returns the y-intercept of the line.
         * @return true on sucess, false otherwise.
         *
         * This functions fails if there are less than 2 recorded counts.
         */
        bool regressionLine(qreal& k, qreal& d);

        /**
         * @brief Draws the slope label.
         * @param w the widget to draw on.
         * @param k the slope of the line.
         * @param d the y-intercept of the line.
         */
        void drawRegressionLineLabel(FigureWidget* w, qreal k, qreal d);

        /**
         * @brief Maps image space to widget space.
         * @param p the point to be mapped.
         * @return the mapped point.
         */
        QPointF is2ws(const QPointF& p) const;

        QPointF is2ws(qreal x, qreal y) const {
            return is2ws(QPointF(x, y));
        }

        // Image containing the fractal structure
        QImage image;

        // Number of black pixels
        int structurePixels;

        // Supported dimensions
        QVector<DimensionDesc> supportedDimensions;

        // Supported grid refinements
        QVector<RefinementDesc> supportedRefinements;

        // Selected dimension
        Dimension dimension;

        // Selected grid refinement
        Refinement refinement;

        // The current state
        State state = State::Idle;

        // Timer for the computation
        QTimer timer;

        // Side length of a cell
        int cellSize;

        // Number of cells
        QSize cellCount;

        // Count of non empty cells
        QVector<QPointF> counts;

        // Non empty cells
        QVector<CellInfo> cells;
};

Q_DECLARE_METATYPE(Processor::Dimension)
Q_DECLARE_METATYPE(Processor::Refinement)

#endif // PROCESSOR_H

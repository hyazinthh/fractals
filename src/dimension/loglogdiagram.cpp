#include "loglogdiagram.h"

#include <QLayout>
#include <QLabel>

// Maximum view extent
static constexpr qreal maxViewSize = 10000;

// Minimum view extent
static constexpr qreal minViewSize = 0.001;

// Scale factor for zooming
static constexpr qreal zoomScaleFactor = 1.15;

LogLogDiagram::LogLogDiagram(const QString& labelX, const QString& labelY, QWidget* parent)
    : QDialog(parent) {

    // Set window title and flags
    setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowSystemMenuHint | Qt::WindowCloseButtonHint);
    setWindowTitle(tr("Log / Log Diagram"));

    // Create layout
    QGridLayout* layout = new QGridLayout(this);
    layout->setSizeConstraint(QLayout::SetFixedSize);
    layout->setSpacing(0);
    this->setLayout(layout);

    // Labels
    QLabel* labelAxisX = new QLabel(labelX, this);
    QLabel* labelAxisY = new QLabel(labelY, this);

    // Add figure widget
    FigureWidget* fw = new FigureWidget(this);
    fw->setMargin(8);

    layout->addWidget(fw, 0, 1, 1, 1);
    layout->addWidget(labelAxisX, 1, 1, 1, 1, Qt::AlignHCenter | Qt::AlignTop);
    layout->addWidget(labelAxisY, 0, 0, 1, 1, Qt::AlignRight | Qt::AlignVCenter);

    connect(fw, &FigureWidget::mouseMoved, this, &LogLogDiagram::processMouseMove);
    connect(fw, &FigureWidget::mousePressed, this, &LogLogDiagram::processMousePress);
    connect(fw, &FigureWidget::mouseWheelMoved, this, &LogLogDiagram::processMouseWheel);
    connect(this, &LogLogDiagram::viewUpdated, fw, &FigureWidget::setView);
    connect(this, &LogLogDiagram::sceneUpdated, fw, &FigureWidget::setScene);
}

void LogLogDiagram::processMouseMove(FigureWidget* sender, QMouseEvent* event) {

    QRectF view = sender->getView();
    QRectF canvas = sender->getCanvasRect();

    QPointF dp = event->localPos() - lastMousePos;
    dp.setX((dp.x() / canvas.width()) * view.width());
    dp.setY((dp.y() / canvas.height()) * view.height());

    if ((event->buttons() & Qt::RightButton) || (event->buttons() & Qt::LeftButton)) {

        view.translate(-dp.x(), dp.y());
        lastMousePos = event->localPos();

        emit viewUpdated(view);
    }

    sender->update();
}

void LogLogDiagram::processMousePress(FigureWidget*, QMouseEvent* event) {

    if (event->buttons() & (Qt::LeftButton | Qt::RightButton)) {
        lastMousePos = event->localPos();
    }
}

void LogLogDiagram::processMouseWheel(FigureWidget* sender, QWheelEvent* event) {

    QRectF view = sender->getView();

    qreal s = zoomScaleFactor;

    if (event->angleDelta().y() > 0) {
        s = (view.width() > minViewSize)  ? (1 / s) : 1;
    } else {
        s = (view.width() < maxViewSize) ? s : 1;
    }

    QPointF c = sender->ws2ds(event->posF());

    view.translate(-c);
    view = QTransform::fromScale(s, s).mapRect(view);
    view.translate(c);

    emit viewUpdated(view);
}

#include "figurewidget.h"

#include <QLocale>
#include <QtMath>

// Number of guide cells
static constexpr int cellCount = 5;

// The margin around axis label text
static constexpr qreal marginText = 5;

// Length of the ticks on the axes
static constexpr qreal tickLength = 5;

FigureWidget::FigureWidget(QWidget *parent)
    : QWidget(parent) {

    setBackgroundRole(QPalette::NoRole);
    setAutoFillBackground(true);
    setMouseTracking(true);

    view = QRect(0, 0, 1, 1);
}

void FigureWidget::computeGeometry() {

    QLocale locale;
    marginAxis = QSizeF(0, 0);

    // Compute guides and labels
    QSizeF cellSize = view.size() / cellCount;

    guidesX.clear();
    guidesY.clear();
    tickLabelsX.clear();
    tickLabelsY.clear();

    // Compute positions of guides in domain space
    // and margins for axis labels
    for (int i = 0; i < cellCount + 2; i++) {

        int min = qFloor(view.left() / cellSize.width());
        qreal x = (min + i) * cellSize.width();

        // Ignore if outside view
        if (x < view.left() || x > view.right()) {
            continue;
        }

        // Label
        TickLabel t;
        t.text = locale.toString(x, 'g', 4);
        t.rect = painter.boundingRect(t.rect, t.text);

        // Update margin
        qreal h = t.rect.height() + 2 * marginText + tickLength;

        if (h > marginAxis.height()) {
            marginAxis.setHeight(h);
        }

        // Save guide line and label
        guidesX.append(QLineF(x, view.top(), x, view.bottom()));
        tickLabelsX.append(t);
    }

    for (int i = 0; i < cellCount + 2; i++) {

        int min = qFloor(view.top() / cellSize.height());
        qreal y = (min + i) * cellSize.height();

        // Ignore if outside view
        if (y < view.top() || y > view.bottom()) {
            continue;
        }

        // Label
        TickLabel t;
        t.text = locale.toString(y, 'g', 4);
        t.rect = painter.boundingRect(t.rect, t.text);

        // Update margin
        qreal w = t.rect.width() + 2 * marginText + tickLength;

        if (w > marginAxis.width()) {
            marginAxis.setWidth(w);
        }

        // Save guide line and label
        guidesY.append(QLineF(view.left(), y, view.right(), y));
        tickLabelsY.append(t);
    }

    // Now that the correct margins are found, we can
    // fix the positions of the guides and labels
    for (int i = 0; i < guidesX.size(); i++) {

        auto& g = guidesX[i];
        g.setPoints(ds2ws(g.p1()), ds2ws(g.p2()));

        auto& t = tickLabelsX[i];
        t.rect = QRectF(g.p1().x(), g.p1().y() + marginText + tickLength + 1, 0, 0);
        t.rect = painter.boundingRect(t.rect, Qt::AlignHCenter | Qt::AlignTop, t.text);
    }

    for (int i = 0; i < guidesY.size(); i++) {

        auto& g = guidesY[i];
        g.setPoints(ds2ws(g.p1()), ds2ws(g.p2()));

        auto& t = tickLabelsY[i];
        t.rect = QRectF(g.p1().x() - marginText - tickLength - 1, g.p1().y(), 0, 0);
        t.rect = painter.boundingRect(t.rect, Qt::AlignRight | Qt::AlignVCenter, t.text);
    }
}

QRectF FigureWidget::getCanvasRect() const {

    QRectF r(margin + marginAxis.width(), margin,
             width() - (2 * margin + marginAxis.width()),
             height() - (2 * margin + marginAxis.height()));

    return r;
}

QPointF FigureWidget::vs2ws(const QPointF& p) const {

    QRectF c = getCanvasRect().adjusted(1, 1, -1, -1);

    QPointF q;
    q.setX(c.left() + p.x() * (c.width() - 1));
    q.setY(c.bottom() - 1 - p.y() * (c.height() - 1));

    return q;
}

QPointF FigureWidget::ws2vs(const QPointF &p) const {

    QRectF c = getCanvasRect().adjusted(1, 1, -1, -1);

    QPointF q;
    q.setX((p.x() - c.left()) / c.width());
    q.setY(1 - (p.y() - c.top()) / c.height());

    return q;
}

bool FigureWidget::drawText(const QPointF& p, const QString& text, const QTextOption& options, const QPen& pen, const QFont& font) {

    if (!painter.isActive()) {
        return false;
    }

    QRectF rect(p, QSizeF(0, 0));
    rect = painter.boundingRect(rect, text, options);

    painter.setPen(pen);
    painter.setFont(font);
    painter.drawText(rect, text, options);

    return true;
}

bool FigureWidget::drawCircle(const QPointF& c, qreal r, const QBrush& brush, bool antialiasing) {

    if (!painter.isActive()) {
        return false;
    }

    painter.setRenderHint(QPainter::Antialiasing, antialiasing);
    painter.setBrush(brush);
    painter.setPen(Qt::NoPen);
    painter.drawEllipse(ds2ws(c), r, r);

    return true;
}

bool FigureWidget::drawLine(const QPointF& a, const QPointF& b, const QPen& pen, bool antialiasing) {

    if (!painter.isActive()) {
        return false;
    }

    painter.setRenderHint(QPainter::Antialiasing, antialiasing);
    painter.setPen(pen);
    painter.drawLine(ds2ws(a), ds2ws(b));

    return true;
}

bool FigureWidget::drawPolyline(QVector<QPointF>& points, const QPen& pen, bool antialiasing) {

    if (!painter.isActive()) {
        return false;
    }

    for (int i = 0; i < points.size(); i++) {
        points[i] = ds2ws(points[i]);
    }

    painter.setRenderHint(QPainter::Antialiasing, antialiasing);
    painter.setPen(pen);
    painter.drawPolyline(points.data(), points.size());

    return true;
}

bool FigureWidget::drawPolygon(QVector<QPointF>& points, const QBrush& brush, bool antialiasing) {

    if (!painter.isActive()) {
        return false;
    }

    for (int i = 0; i < points.size(); i++) {
        points[i] = ds2ws(points[i]);
    }

    painter.setRenderHint(QPainter::Antialiasing, antialiasing);
    painter.setBrush(brush);
    painter.setPen(Qt::NoPen);
    painter.drawPolygon(points.data(), points.size());

    return true;
}

void FigureWidget::paintEvent(QPaintEvent*) {

    painter.begin(this);

    painter.setPen(Qt::black);
    painter.setBrush(Qt::white);
    painter.setRenderHint(QPainter::Antialiasing, false);

    // Update geometry
    computeGeometry();

    // Draw canvas
    QRectF canvas = getCanvasRect();
    painter.drawRect(canvas.adjusted(0, 0, -1, -1));

    // Draw guide lines
    QPen pen(Qt::gray);
    pen.setStyle(Qt::DotLine);
    painter.setPen(pen);

    for (QLineF& l : guidesX) {
        painter.drawLine(l);
    }

    for (QLineF& l : guidesY) {
        painter.drawLine(l);
    }

    // Axis labels
    painter.setPen(Qt::black);

    for (QLineF& l : guidesX) {
        painter.drawLine(l.p1().x(), l.p1().y() + 1,
                         l.p1().x(), l.p1().y() + tickLength + 1);
    }

    for (TickLabel& l : tickLabelsX) {
        painter.drawText(l.rect, Qt::AlignHCenter | Qt::AlignTop, l.text);
    }

    for (QLineF& l : guidesY) {
         painter.drawLine(l.p1().x() - tickLength - 1, l.p1().y(),
                          l.p1().x() - 1, l.p1().y());
    }

    for (TickLabel& l : tickLabelsY) {
        painter.drawText(l.rect, Qt::AlignRight | Qt::AlignVCenter, l.text);
    }

    // Draw scene
    painter.setClipRect(canvas.adjusted(1, 1, -1, -1));

    if (scene) {
        scene->draw(this);
    }

    painter.end();
}

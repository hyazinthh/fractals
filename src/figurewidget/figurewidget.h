#ifndef FIGUREWIDGET_H
#define FIGUREWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QVector>
#include <QMouseEvent>
#include <QWheelEvent>

/**
 * @brief Widget for plotting functions.
 *
 * Three distinct coordinate systems are used:
 *
 * - Domain space
 * - View space
 * - Widget space
 *
 * Domain space is the coordinate system in which the graphical objects of the
 * figure are defined. View space is defined as the unit square [0, 1]^2. The mapping
 * between domain and view space is defined by the current position and size of the view.
 * Widget space is the local coordinate system of the widget, where (0,0)
 * and (width(), height()) map the top left and bottom right corners respectively.
 */
class FigureWidget : public QWidget
{
    Q_OBJECT

    public:

        /**
         * @brief Interface for classes to be rendered in a figure widget.
         */
        class Scene {
            public:
                virtual ~Scene() = default;

                /**
                 * @brief Called when the figure widget is to be drawn to.
                 * @param w the widget to be updated.
                 */
                virtual void draw(FigureWidget* w) = 0;
        };

        /**
         * @brief Constructs a new figure widget.
         * @param parent the parent of the widget.
         */
        explicit FigureWidget(QWidget* parent = Q_NULLPTR);

        /**
         * @brief Returns the minimum size of the widget.
         * @return the minimum size.
         */
        QSize minimumSizeHint() const Q_DECL_OVERRIDE {
            return QSize(500, 500);
        }

        /**
         * @brief Returns the preferred size of the widget.
         * @return the prefrerred size.
         */
        QSize sizeHint() const Q_DECL_OVERRIDE {
            return minimumSizeHint();
        }

        /**
         * @brief Sets the margin around the widget.
         * @param margin the margin in pixels.
         */
        void setMargin(int margin) {
            this->margin = margin;
        }

        /**
         * @brief Draws text.
         * @param p the point to draw to in widget space.
         * @param text the text string to draw.
         * @param options the options used for drawing.
         * @param pen the pen used for drawing.
         * @param font the font used for drawing.
         * @return true on success, false otherwise.
         *
         * Can only be called from within paintEvent(), fails otherwise.
         */
        bool drawText(const QPointF& p, const QString& text, const QTextOption& options,
                      const QPen& pen = QPen(Qt::black), const QFont& font = QFont());

        bool drawText(qreal x, qreal y, const QString& text, const QTextOption& options,
                      const QPen& pen = QPen(Qt::black), const QFont& font = QFont()) {
            return drawText(QPointF(x, y), text, options, pen, font);
        }

        /**
         * @brief Draws a circle.
         * @param c the center of the circle.
         * @param r the radius of the circle.
         * @param brush the brush used for drawing.
         * @param antialiasing true if antialiasing is to be used, false otherwise.
         * @return true on success, false otherwise.
         *
         * Can only be called from within paintEvent(), fails otherwise.
         */
        bool drawCircle(const QPointF& c, qreal r, const QBrush& brush = QColor(Qt::black), bool antialiasing = true);

        bool drawCircle(qreal x, qreal y, qreal r, const QBrush& brush = QColor(Qt::black), bool antialiasing = true) {
            return drawCircle(QPointF(x, y), r, brush, antialiasing);
        }

        /**
         * @brief Draws a line.
         * @param a the first point of the line.
         * @param b the second point of the line.
         * @param pen the pen used for drawing.
         * @param antialiasing true if antialiasing is to be used, false otherwise.
         * @return true on success, false otherwise.
         *
         * Can only be called from within paintEvent(), fails otherwise.
         */
        bool drawLine(const QPointF& a, const QPointF& b, const QPen& pen = QColor(Qt::black), bool antialiasing = true);

        bool drawLine(qreal x1, qreal y1, qreal x2, qreal y2, const QPen& pen = QColor(Qt::black), bool antialiasing = true) {
            return drawLine(QPointF(x1, y1), QPointF(x2, y2), pen, antialiasing);
        }

        /**
         * @brief Draws a line strip.
         * @param points the points of the line strip.
         * @param pen the pen used for drawing.
         * @param antialiasing true if antialiasing is to be used, false otherwise.
         * @return true on success, false otherwise.
         *
         * Can only be called from within paintEvent(), fails otherwise.
         */
        bool drawPolyline(QVector<QPointF>& points, const QPen& pen = QColor(Qt::black), bool antialiasing = true);

        /**
         * @brief Draws a filled polygon.
         * @param points the points of the polygon.
         * @param brush the brush used for drawing.
         * @param antialiasing true if antialiasing is to be used, false otherwise.
         * @return true on success, false otherwise.
         *
         * Can only be called from within paintEvent(), fails otherwise.
         */
        bool drawPolygon(QVector<QPointF>& points, const QBrush& brush = QColor(Qt::black), bool antialiasing = true);

        /**
         * @brief Gets the current view.
         * @return the view.
         */
        const QRectF& getView() const {
            return view;
        }

        /**
         * @brief Returns the bounding rectangle of the canvas (including 1px borders).
         * @return the canvas rectangle.
         */
        QRectF getCanvasRect() const;

        /**
         * @brief Maps domain space to view space.
         * @param p the point to be mapped.
         * @return the mapped point.
         */
        QPointF ds2vs(const QPointF& p) const {
            return QPointF((p.x() - view.left()) / view.width(),
                           (p.y() - view.top()) / view.height());
        }

        QPointF ds2vs(qreal x, qreal y) const {
            return ds2vs(QPointF(x, y));
        }

        /**
         * @brief Maps view space to widget space.
         * @param p the point to be mapped.
         * @return the mapped point.
         */
        QPointF vs2ws(const QPointF& p) const;

        QPointF vs2ws(qreal x, qreal y) const {
            return vs2ws(QPointF(x, y));
        }

        /**
         * @brief Maps domain space to widget space.
         * @param p the point to be mapped.
         * @return the mapped point.
         */
        QPointF ds2ws(const QPointF& p) const {
            return vs2ws(ds2vs(p));
        }

        QPointF ds2ws(qreal x, qreal y) const {
            return ds2ws(QPointF(x, y));
        }

        /**
         * @brief Maps view space to domain space.
         * @param p the point to be mapped.
         * @return the mapped point.
         */
        QPointF vs2ds(const QPointF& p) const {
            return QPointF(view.left() + p.x() * view.width(),
                           view.top() + p.y() * view.height());
        }

        QPointF vs2ds(qreal x, qreal y) const {
            return vs2ds(QPointF(x, y));
        }

        /**
         * @brief Maps widget space to view space.
         * @param p the point to be mapped.
         * @return the mapped point.
         */
        QPointF ws2vs(const QPointF& p) const;

        QPointF ws2vs(qreal x, qreal y) const {
            return ws2vs(QPointF(x, y));
        }

        /**
         * @brief Maps widget space to domain space.
         * @param p the point to be mapped.
         * @return the mapped point.
         */
        QPointF ws2ds(const QPointF& p) const {
            return vs2ds(ws2vs(p));
        }

        QPointF ws2ds(qreal x, qreal y) const {
            return ws2ds(QPointF(x, y));
        }

    signals:

        /**
         * @brief Emitted when a mouse button is pressed.
         * @param sender the sender, i.e. this.
         * @param event the event data.
         */
        void mousePressed(FigureWidget* sender, QMouseEvent* event);

        /**
         * @brief Emitted when a mouse button is released.
         * @param sender the sender, i.e. this.
         * @param event the event data.
         */
        void mouseReleased(FigureWidget* sender, QMouseEvent* event);

        /**
         * @brief Emitted when the mouse is moved.
         * @param sender the sender, i.e. this.
         * @param event the event data.
         */
        void mouseMoved(FigureWidget* sender, QMouseEvent* event);

        /**
         * @brief Emitted when the mouse wheel is turned.
         * @param sender the sender, i.e. this.
         * @param event the event data.
         */
        void mouseWheelMoved(FigureWidget* sender, QWheelEvent* event);

    public slots:

        /**
         * @brief Sets the pointer to the scene to render.
         * @param scene the scene.
         */
        void setScene(Scene* scene) {
            this->scene = scene;
            update();
        }

        /**
         * @brief Sets the current view.
         * @param view
         */
        void setView(const QRectF& view) {
            this->view = view;
            update();
        }

    protected:
        /**
         * @brief Called when widget is repainted.
         * @param event the event data.
         */
        void paintEvent(QPaintEvent* event) Q_DECL_OVERRIDE;

        /**
         * @brief Called when a mouse button is pressed.
         * @param event the mouse event.
         */
        void mousePressEvent(QMouseEvent* event) Q_DECL_OVERRIDE {
            emit mousePressed(this, event);
        }

        /**
         * @brief Called when a mouse button is released.
         * @param event the mouse event.
         */
        void mouseReleaseEvent(QMouseEvent* event) Q_DECL_OVERRIDE {
            emit mouseReleased(this, event);
        }

        /**
         * @brief Called when the mouse is moved.
         * @param event the mouse event.
         */
        void mouseMoveEvent(QMouseEvent* event) Q_DECL_OVERRIDE {
            emit mouseMoved(this, event);
        }

        /**
         * @brief Called when the mouse wheel is turned.
         * @param event the wheel event.
         */
        void wheelEvent(QWheelEvent* event) Q_DECL_OVERRIDE {
            emit mouseWheelMoved(this, event);
        }

    private:

        struct TickLabel {
            QRectF rect;
            QString text;
        };

        /**
         * @brief Computes margins and guides.
         */
        void computeGeometry();

        // The painter of the widget
        QPainter painter;

        // The margin used for labeling the axes
        QSizeF marginAxis;

        // The margin around the widget
        int margin = 16;

        // Guide lines
        QVector<QLineF> guidesX, guidesY;

        // Labels for guide line ticks
        QVector<TickLabel> tickLabelsX, tickLabelsY;

        // The current view
        QRectF view;

        // The scene to be drawn in the figure
        Scene* scene = Q_NULLPTR;
};

#endif // FIGUREWIDGET_H

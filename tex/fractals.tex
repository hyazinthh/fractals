\documentclass[titlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{cleveref}
\usepackage{subcaption}

\begin{document}

	\title{\textbf{Fractals Exercise}}

	\author{
	  Mautner, Martin\\
	  1127229\\066 932\\\\
	  \texttt{e1127229@student.tuwien.ac.at}
	}

\maketitle

\section{Fractal dimensions (5.1)}
This application computes the box counting and information dimension for binary images.

\subsection{Algorithm}
The box counting dimension is computed by counting how many cells $N(s)$ of side length $s$ are required to cover the fractal structure. This is done by superimposing the image with a regular grid of varying cell size. The resulting data points are plotted in a log/log diagram with the x-axis as $\ln \left ( \frac{1}{s} \right )$ and the y-axis as $\ln N(s)$. Then a regression line for the data points is computed using the least squares method. The box counting dimension is the slope of that line.

The information dimension~\cite{peitgen2006} is computed similarly, however $\ln N(s)$ is replaced by
\begin{equation}
	I(s) = \sum_{k=1}^{N(s)} \mu(B_k) \ln \left ( \frac{1}{\mu(B_k)} \right )
\end{equation}
where $B_k$ are the non-empty cells and $\mu$ is the natural measure, i.e. the ratio of pixels of the structure that are contained within the cell.

A problem with this simple algorithm is that only regular grids are considered and thus its alignment affects the outcome. Moreover, different refinement strategies will lead to different results. This can be seen when testing the application with the two square pictures. Using exponential refinement, the grid will perfectly align with one picture but not with the other. In the first case, this will lead to the correct solution of 2, whereas in the latter case a dimension of less than 2 will be returned. A more precise approach would be to shift around the grid in all possible ways and use the minimum coverage. However, the increased complexity would probably require a GPU-based implementation for a real-time application.

\subsection{Usage}
First, an image has to be loaded by clicking the \emph{Load} button or selecting \emph{Open} from the file menu. Black pixels count as part of the structure, any other color is treated as background. Various options can be selected on the right side of the window including the dimension to be computed. The symbolic buttons above the \emph{Load} button can be used to control the computation. The slider controls the speed of the animation.

Once the animation is finished, a dialog opens showing the log/log diagram. Here the individual data points as well as the regression line are shown. The slope of the line (denoted as $k$), represents the computed dimension.

\section{Graphical iteration (5.3)}
The application visualizes the dynamic behavior of various functions.

\subsection{Algorithm}
Given a function $f_a(x)\colon X \rightarrow Y$ with $Y \subseteq X$ and a starting point $x_0 \in X$, the application computes 
\begin{equation}
	x_{n+1} = f_a(x_n)
\end{equation}
for $n \to \infty$. The behavior of the orbit $x_0, x_1, \dots, x_n$ depends on the function and the starting point $x_0$. It may either converge towards a fix point or show chaotic behavior. This process can be visualized by drawing a vertical line from $\left (x_0, 0 \right )$ to $\left (x_0, f_a(x_0) \right )$. Then a horizontal line is drawn towards the median, which corresponds to mapping $f_a(x_0)$ to the x-axis giving $x_1$. Subsequently, the procedure is continued with $x_1$.

This algorithm can also be easily extended to work with intervals instead of single points.

\subsection{Usage}
The function and its parameter $a$ can be selected on the right hand side of the interface. Below that are controls for the iteration itself, including a slider for the animation speed and symbolic buttons for playing, pausing, stepping and stopping the animation. First the user has to press the \emph{Select} button in order to select a starting point or interval for the iteration. Once a valid interval or point is selected, the iteration can be controlled using the buttons above the \emph{Select} button. To improve visibility only the latest 16 steps of the orbit are visualized. The iteration is terminated when the function or its parameter are changed, the stop button is pressed or the iteration reached a value that exceeds the machine's precision.

The view can be panned by dragging with the left or right mouse button. The mouse wheel can be used to zoom in and out.

\bibliographystyle{acm}
\bibliography{fractals}

\end{document}

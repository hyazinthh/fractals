TEMPLATE = subdirs

SUBDIRS += \
    iteration \
    dimension \
    figurewidget \

iteration.subdir = src/iteration
dimension.subdir = src/dimension
figurewidget.subdir = src/figurewidget

iteration.depends = figurewidget
dimension.depnds = figurewidget
